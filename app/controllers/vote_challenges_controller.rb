class VoteChallengesController < ApplicationController
  before_action :set_vote_challenge, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /vote_challenges
  # GET /vote_challenges.json
  def index
    @vote_challenges = VoteChallenge.all
  end

  # GET /vote_challenges/1
  # GET /vote_challenges/1.json
  def show
  end

  # GET /vote_challenges/new
  def new
    @vote_challenge = VoteChallenge.new
  end

  # GET /vote_challenges/1/edit
  def edit
  end

  # POST /vote_challenges
  # POST /vote_challenges.json
  def create
    @vote_challenge = VoteChallenge.new(vote_challenge_params)

    respond_to do |format|
      if @vote_challenge.save
        format.html { redirect_to @vote_challenge, notice: 'Vote challenge was successfully created.' }
        format.json { render :show, status: :created, location: @vote_challenge }
      else
        format.html { render :new }
        format.json { render json: @vote_challenge.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vote_challenges/1
  # PATCH/PUT /vote_challenges/1.json
  def update
    respond_to do |format|
      if @vote_challenge.update(vote_challenge_params)
        format.html { redirect_to @vote_challenge, notice: 'Vote challenge was successfully updated.' }
        format.json { render :show, status: :ok, location: @vote_challenge }
      else
        format.html { render :edit }
        format.json { render json: @vote_challenge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vote_challenges/1
  # DELETE /vote_challenges/1.json
  def destroy
    @vote_challenge.destroy
    respond_to do |format|
      format.html { redirect_to vote_challenges_url, notice: 'Vote challenge was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vote_challenge
      @vote_challenge = VoteChallenge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vote_challenge_params
      params.require(:vote_challenge).permit(:user_id, :challenge_id)
    end
end
