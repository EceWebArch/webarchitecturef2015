Rails.application.routes.draw do
  root                'home#index'
  get 'about'     =>  'home#about'
  get 'contact'   =>  'home#contact'
  get 'faq'       =>  'home#faq'
  get 'policy'    =>  'home#policy'
  
#  post 'contact'  =>  'home#contact'

  devise_for :users, :controllers => { omniauth_callbacks: 'omniauth_callbacks' }
 
  resources :users do
    resources :groups
  end
  resources :groups do
    resources :users
  end
  resources :contributions
  resources :vote_challenges
  resources :vote_comments
  resources :comments
  resources :challenges
  resources :categories do
    resources :challenges
  end

match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup
end
