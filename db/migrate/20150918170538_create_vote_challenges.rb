class CreateVoteChallenges < ActiveRecord::Migration
  def change
    create_table :vote_challenges do |t|
      t.integer :user_id
      t.integer :challenge_id

      t.timestamps null: false
    end
  end
end
