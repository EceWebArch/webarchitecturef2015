class AddDetailsToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :min_num, :integer
    add_column :challenges, :max_num, :integer
  end
end
